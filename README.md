# Privamics
Privamics is a strip webcomic around privacy. I'm using Krita to draw and paint and Inkscape to create dialog and page layout.
In this repo you'll find :
* a static kra zip file
* SVG files (fr and en at least)
* the PNG file extracted from krita files linked to SVG files

Everything is released under Creative Common CC-BY-SA 4.0 licence

## Episodes

### Episode 01
#### Nothing to hide
![](./ep01/rienacacher-e01_en_web.png)

#### Rien à cacher
![](./ep01/rienacacher-e01_web.png)
